# 日记与待办2.X开源项目：简洁高效的Markdown日记软件 | DiaryTodo 2.X OpenSource Project : A Simple but effective diary application of Markdown

[![Gitee 仓库](https://img.shields.io/badge/Gitee-仓库-C71D23?logo=gitee)](https://gitee.com/StarVase/DiaryTodo2)
[![Github 仓库](https://img.shields.io/badge/Github-仓库-0969DA?logo=github)](https://github.com/StarVase/DiaryTodo2)

![icon](app/src/main/res/mipmap-xxxhdpi/ic_launcher.png)

## 注意 | Alert

请不要在 Github 内直接更改此仓库，因为 Github 的仓库是由 Gitee 镜像过去的。   
Please not to change the repository directly in Github, because Github's repository is mirrored by Gitee。


## 介绍 | Introduction

日记与待办2.X：

这是一款支持Markdown、简洁高效的安卓记事工具。  

该软件遵循[GPL3.0](https://gitee.com/StarVase/DiaryTodo2/blob/master/license.txt)协议开源。

 **包名：** com.StarVase.diaryTodo  
 **版本：** 2.23H2.15 Release (2023100801)  
 **大小：** 7.3M (7637199)  

Page：[https://diarytodo.starvase.cn/](https://diarytodo.starvase.cn/)


## 开发 | Development

编写语言：java、lua、JavaScript

开发工具：aidelua、mlua、aide、Androidstudio...  
如果您需要编译安装，请使用AIDE Pro打包。  

#### AIDE Pro

[![官网 (推荐)](https://img.shields.io/badge/官网-推荐-28B6F6)](https://www.aidepro.top/)
[![蓝奏云 (官方)](https://img.shields.io/badge/蓝奏云-v2.6.45-FF6600?logo=icloud&logoColor=white)](https://www.lanzouy.com/b00zdhbeb)


## 下载 | Download
转到[发行版](https://gitee.com/StarVase/DiaryTodo2/releases/tag/2.24H2.2_Release)下载
去[酷安](http://www.coolapk.com/apk/273947)下载  

## 注意事项

1. AIDE 必须使用 `AIDE高级设置版本` ，否则无法打开 `重定义Apk路径`
2. AIDE 必须打开 `重定义Apk路径` ，否则会导致 APK 错误
3. AIDE 最好关闭 `adrt调试文件`
4. 不是必须用 AIDE 编译，也可以使用 Android Studio 编译  


## 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


